module Foldfib where

mfoldl :: (a -> b -> a) -> a -> [b] -> a
mfoldl f i [] = i
mfoldl f i (x:xs) = mfoldl f (f i x) xs

msum,mprd :: Num a => [a] -> a
msum = mfoldl (+) 0
mprd = mfoldl (*) 1

mfoldr :: (a -> b -> b) -> b -> [a] -> b
mfoldr f i [] = i
mfoldr f i (x:xs) = f x (mfoldr f i xs)

mqsort :: Ord a => [a] -> [a]
mqsort [] = []
mqsort (x:xs) = mqsort smaller ++ [x] ++ mqsort bigger where
                        smaller = filter (<= x) xs
                        bigger = filter (> x) xs 

fibs = 0:1:zipWith(+) fibs (tail fibs)

-- https://www.cis.upenn.edu/~cis194/spring13/hw/04-higher-order.pdf
-- exercise no.3 (as homework for section 4 of FPAms Haskell course 2021
xorn :: [Bool] -> Bool
xorn xs = odd $ foldr count 0 xs where
                   count :: Bool -> Int -> Int
                   count b n | b == True = n + 1
                             | otherwise = n
xor :: [Bool] -> Bool
xor xs = foldr exor False xs 

exor :: Bool -> Bool -> Bool
exor x y | x == False = y
         | otherwise = not y

mmap :: (a -> b) -> [a] -> [b]
mmap f = foldr (\x -> (f x :)) []
{-
*Foldfib> mmap (+1) [1..5]
[2,3,4,5,6]
*Foldfib> mmap (sqrt . fromInteger) [1..5]
[1.0,1.4142135623730951,1.7320508075688772,2.0,2.23606797749979]
-}
myFoldl :: (a -> b -> a) -> a -> [b] -> a
myFoldl f base xs = foldr (flip f) base (reverse xs)
{-
*Foldfib> myFoldl (/) 120.0 [1,2,3,4]
5.0
*Foldfib> foldl (/) 120.0 [1,2,3,4]
5.0
-}
