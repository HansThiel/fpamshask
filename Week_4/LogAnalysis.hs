{-# OPTIONS_GHC -Wall #-}
module LogAnalysis where

import Log
import Data.Maybe (fromJust)

-- ex. 1 Note: read n should test for failure
--             variables should be renamed for clarity
getMsT :: [String] -> Maybe (MessageType, [String])
getMsT (w:n:ws) = case w of
                    "I" -> Just (Info, (n:ws))
                    "W" -> Just (Warning, (n:ws))
                    "E" -> Just ((Error (read n)), ws)
                    _   -> Nothing
getMsT _ = Nothing
                        
getTst :: [String] -> Maybe (TimeStamp, [String])
getTst (n:ws) = Just (read n, ws)
getTst _ = Nothing

getRst :: [String] -> Maybe String
getRst [] = Nothing
getRst ws = Just (unwords ws)

getAll :: [String] -> Maybe (MessageType, TimeStamp, String)
getAll ws = do
              (mst,tsl) <- getMsT ws
              (ts, rss) <- getTst tsl
              s         <- getRst rss
              return (mst,ts,s)


parseMessage :: String -> LogMessage
parseMessage s | p == Nothing = Unknown s
               | otherwise = (getMess . fromJust) p                
               where p = (getAll . words) s
                     getMess (x,y,z) = LogMessage x y z

parse:: String -> [LogMessage]
parse logls = [parseMessage ln | ln <- lines logls]

-- ex.2
 
newtype Stamp = Stamp LogMessage deriving (Show, Eq)

instance Ord Stamp where
 compare (Stamp (LogMessage _ t1 _)) (Stamp (LogMessage _ t2 _)) = compare t1 t2
 compare _ _ = error "Cannot compare LogMessage timestamps" 

insert :: LogMessage -> MessageTree -> MessageTree
insert (Unknown _) t = t
insert m Leaf = Node Leaf m Leaf
insert m (Node left clm right)
  | Stamp m <= Stamp clm = Node (insert m left) clm right
  | otherwise = Node left clm (insert m right)
-- ex. 3
build :: [LogMessage] -> MessageTree
build ls = foldr insert Leaf ls
{- uh..oh
-- ex. 4
extract :: MessageTree -> Maybe (LogMessage, MessageTree)
extract Leaf = Nothing
extract (Node Leaf lm Leaf) = Just (lm, Leaf)
extract (Node Leaf lm right) = Just (lm, right)
extract (Node (Node Leaf dnm dnright) upm upright) = 
                        Just (dnm, (Node dnright upm upright))
extract (Node left _ _) = extract left

inOrder :: MessageTree -> [LogMessage]
inOrder  = unfoldr extract 
-}
-- taken from Tjeerd Hans!!!
inOrder :: MessageTree -> [LogMessage]
inOrder Leaf = []
inOrder (Node l m r) = inOrder l ++ [m] ++ inOrder r
-- Now, this is Haskell...

-- ex.5
logTest :: LogMessage -> Bool
logTest (LogMessage typ _ _) = 
  let check :: MessageType -> Bool
      check (Error n) = if n >= 50 then True
                                   else False
      check _ = False
  in check typ 
logTest (Unknown _) = True -- at best, a bad patch!!!

getMessage :: LogMessage -> String
getMessage (LogMessage _ _ str) = str
getMessage ms@(Unknown _) =  show ms

whatWentWrong :: [LogMessage] -> [String]
whatWentWrong  = (map getMessage) . inOrder . build . (filter logTest)


