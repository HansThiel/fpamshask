module Zigzag where

import Data.List (sort)

-- find the first occurrence, if any, of two numbers
-- each in a seperate list, which add up to a given number
-- the first list MUST be sorted from small to large (sh)
-- the second list MUST be sorted from large to small (lh)
  
zigzag :: [Int] -> [Int] -> Int -> [Int]
zigzag [] _ _ = []
zigzag _ [] _ = []
zigzag sh@(x:xs) lh@(y:ys)  m | (x + y) == m = [x,y] 
                              | (x + y) > m = zigzag sh ys m
                              | otherwise = zigzag xs lh m

-------------------------------------------------------------------------
twoFactor :: [Int] -> Int -> [Int]
twoFactor ls m = zigzag smhead lghead m where
                      smhead = sort ls         -- from lowest to highest
                      lghead = reverse smhead  -- now from highest to lowest

zigzagzog :: [Int] -> [Int] -> [Int] -> [Int]
zigzagzog _ _ [] = []
zigzagzog sh lh (m:ms) -- sh and lh Must be sorted as above
      | zigzag sh lh m == [] = zigzagzog sh lh ms
      | otherwise = zigzag sh lh m


thrFactor ls som = zigzagzog smhead lghead bounds where
                      smhead = sort ls
                      lghead = reverse smhead
                      bounds = map (\x -> som - x) ls 

