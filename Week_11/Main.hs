module Main where

import System.IO
import Zigzag (twoFactor,thrFactor)

getNumList :: String -> [Int]
getNumList s = map read  (lines s)

main = do 
      contents <- readFile "advCodeNumList"
      let numList = getNumList contents
--          res = twoFactor numList 2020
          tmp = thrFactor numList 2020
          res = (2020 - (sum tmp)) : tmp
      putStrLn (show res)

--[(833,1187)] for two factors
--[1237,272,511] for three factors

