<<<<<<< HEAD
{-# LANGUAGE TypeSynonymInstances #-}
{-# LANGUAGE FlexibleInstances #-}
=======
--{-# Language FlexibleInstances #-}
>>>>>>> fpams_optional
{-# OPTIONS_GHC -Wall #-}

module Calc where 

import Parser (parseExp)
<<<<<<< HEAD
import qualified StackVM as S
=======
--import qualified Data.Map as M
>>>>>>> fpams_optional

--ex.1
eval :: VarExprT -> Integer
eval (Lit n) = n
eval (Add e1 e2) = (eval e1) + (eval e2)
eval (Mul e1 e2) = (eval e1) * (eval e2)

-- ex.2
evalStr :: String -> Maybe Integer
evalStr s  = eval <$> parseExp Lit Add Mul s

-- ex.3
class Expr a where
   lit :: Integer -> a
   add :: a -> a -> a
   mul :: a -> a -> a

--ex.4

instance Expr Integer where
    lit = id
    add = (+)
    mul = (*)

instance Expr Bool where
    lit n | n <= 0 = False
          | otherwise = True
    add = (||)
    mul = (&&)

newtype MinMax = MinMax Integer deriving (Eq, Show)

instance Expr MinMax where
    lit = MinMax
    add (MinMax x) (MinMax y) = MinMax (max x y)
    mul (MinMax x) (MinMax y) = MinMax (max x y)

newtype Mod7 = Mod7 Integer deriving (Eq, Show)

instance Expr Mod7 where
    lit n = Mod7 (mod n 7) 
    add (Mod7 x) (Mod7 y)= Mod7 (mod (x + y) 7) -- (a + b) mod c == 
    mul (Mod7 x) (Mod7 y)= Mod7 (mod (x * y) 7) -- ((a mod c) + (b mod c)) mod c
                                                

testExp :: Expr a => Maybe a
testExp = parseExp lit add mul "(3 * -4) + 5"  

testInteger :: Maybe Integer
testInteger = testExp -- :: Maybe Integer
testBool :: Maybe Bool  
testBool    = testExp -- :: Maybe Bool
testMM  :: Maybe MinMax    
testMM      = testExp -- :: Maybe MinMax
testSat :: Maybe Mod7     
testSat     = testExp -- :: Maybe Mod7 

<<<<<<< HEAD
-- ex. 5

instance Expr (S.Program) where
   lit n = [S.PushI n]
   add e1 e2 = e1 ++ e2 ++ [S.Add]
   mul e1 e2 = e1 ++ e2 ++ [S.Mul]

testProgram :: Maybe S.Program
testProgram = testExp

compile :: String -> Maybe S.Program -- is necessary here (see above)
compile  = parseExp lit add mul 
=======
-- ex. 6

data VarExprT = Lit Integer
              | Add VarExprT VarExprT
              | Mul VarExprT VarExprT
              | Var (String -> Integer)

instance Expr VarExprT where
     lit = Lit
     add = Add
     mul = Mul

class HasVars a where
  var :: String -> a

--instance HasVars VarExprT where
--     var  = Var . id 

--instance HasVars (M.Map String Integer -> Maybe Integer) where
--      var  = M.lookup varTable 
>>>>>>> fpams_optional

